// Soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var stringPertama1 = pertama.substr(0, 5);
var stringPertama2 = pertama.substr(12, 7);

var stringKedua1 = kedua.substr(0, 8);
var stringKedua2 = kedua.substr(8, 10).toUpperCase();

var gabungStringPertama = stringPertama1.concat(stringPertama2);
var gabungStringKedua = stringKedua1.concat(stringKedua2);

var gabungStringPertamaKedua = gabungStringPertama.concat(gabungStringKedua);
console.log(gabungStringPertamaKedua); //saya senang belajar JAVASCRIPT

// Soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var IntkataPertama = parseInt(kataPertama);
var IntkataKedua = parseInt(kataKedua);
var IntKataKetiga = parseInt(kataKetiga);
var IntkataKeempat = parseInt(kataKeempat);

var OperasiMatematika =
  IntkataPertama + (IntkataKedua + 2) + IntKataKetiga + IntkataKeempat;

console.log(OperasiMatematika);

// Soal 3
var kalimat = "wah javascript itu keren sekali";

var kataPertama = kalimat.substring(0, 3); // wah
var kataKedua = kalimat.substring(4, 14); // javascript
var kataKetiga = kalimat.substring(15, 18); // itu
var kataKeempat = kalimat.substring(19, 24); // keren
var kataKelima = kalimat.substring(25, 32); // sekali

console.log("Kata Pertama: " + kataPertama);
console.log("Kata Kedua: " + kataKedua);
console.log("Kata Ketiga: " + kataKetiga);
console.log("Kata Keempat: " + kataKeempat);
console.log("Kata Kelima: " + kataKelima);
